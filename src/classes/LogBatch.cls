/* Batch used to delete log__c records. The default number of records to delete is 200, but it can be set through the parameter passed to the constructor,
 *
 *  
 */

global class LogBatch implements Database.Batchable<sObject>{
    Integer recordsToDelete = 200;
    
    public LogBatch(Integer recordsToDelete){
        this.recordsToDelete = recordsToDelete;
    }
        
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id FROM Log__c ORDER BY CreatedDate ASC LIMIT ' + String.valueOf(recordsToDelete);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        delete scope;
    }
    
    global void finish(Database.BatchableContext BC){}
    
}