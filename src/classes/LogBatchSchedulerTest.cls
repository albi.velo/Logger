@isTest
public class LogBatchSchedulerTest {
    
    @isTest
    private static void executeBatchTest(){
        Integer recordsLeft = Integer.valueOf(System.Label.Logs_NumberOfRecordsToPreserve);
        
        List<Log__c> l = new List<Log__c>();
        for(Integer i=0; i<recordsLeft+10; i++)
            l.add(new Log__c());
        insert l;
        
        Integer count = [SELECT count() FROM Log__c];
        
        Test.startTest();
        Database.executeBatch(new LogBatch(count-recordsLeft));   
        Test.stopTest();
        system.assertEquals(recordsLeft,[SELECT count() FROM Log__c]);
    }
    
    @isTest
    private static void executeSchedulerTest(){
        Integer recordsLeft = Integer.valueOf(System.Label.Logs_NumberOfRecordsToPreserve);
        
        List<Log__c> l = new List<Log__c>();
        for(Integer i=0; i<recordsLeft+10; i++)
            l.add(new Log__c());
        insert l;
        
        
        
        Datetime now = Datetime.now();
        String CRON_EXP = '0 '+ now.minute() + ' * ' + now.day() + ' ' + now.month() + ' ? ' + now.year();
        Test.startTest();
        system.schedule('Test', CRON_EXP, new LogScheduler());   
        Test.stopTest();
    }
}