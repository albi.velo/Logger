/*
 * Class used to schedule the execution of the log-deleting batch. The recordsToPreserve parameters is used to set the maximum number of Log__c records to keep in the org,
 * and is editable through the "Logs_NumberOfRecordsToPreserve" custom label
 */
global class LogScheduler implements Schedulable {    
    
    global void execute(SchedulableContext scMain) {
        //number of log__c records you want to preserve
        Integer recordsToPreserve = Integer.valueOf(System.Label.Logs_NumberOfRecordsToPreserve);
        Integer count = [SELECT count() FROM Log__c];
                             
        if(count > recordsToPreserve){
            LogBatch b = new LogBatch(count - recordsToPreserve);
            Database.executeBatch(b);
        }
    }    
}